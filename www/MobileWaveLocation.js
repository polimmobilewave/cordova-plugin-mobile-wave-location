var exec = require('cordova/exec');


MobileWave.publishLocationInfo = function(options, callback) {
  var onNavigatorSuccess = function(position) {
    var data = {};
    data["coords"] = {};
    data["coords"]["latitude"] = position["coords"]["latitude"];
    data["coords"]["longitude"] = position["coords"]["longitude"];
    data["coords"]["altitude"] = position["coords"]["altitude"];
    data["coords"]["accuracy"] = position["coords"]["accuracy"];
    data["coords"]["altitudeAccuracy"] = position["coords"]["altitudeAccuracy"];
    data["coords"]["heading"] = position["coords"]["heading"];
    data["coords"]["speed"] = position["coords"]["speed"];
    data["timestamp"] = position["timestamp"];

    MobileWave.publish(data, callback);
  };
  navigator.geolocation.getCurrentPosition(onNavigatorSuccess, null, options);
};
